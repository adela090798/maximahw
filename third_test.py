import Lesson23_3
import unittest


class MyTest(unittest.TestCase):

    def setUp(self) -> None:
        print(f'Set up for [{self.shortDescription()}]')

    def tearDown(self) -> None:
        print(f"Tear down for [{self.shortDescription()}]")

    def test_name(self):
        """Name operation test"""
        self.assertEqual(Lesson23_3.tom.say_name(), "Tom")

    def test_age(self):
        """Age operation test"""
        self.assertNotEqual(Lesson23_3.tom.say_age(), 20)

    def test_height(self):
        """Height operation test"""
        self.assertGreater(Lesson23_3.tom.say_height(), 100)

    def test_weight(self):
        """Weight operation test"""
        self.assertLess(Lesson23_3.tom.say_weight(), 100)

    def test_less(self):
        """Result operation test"""
        self.assertTrue(Lesson23_3.Person.less)


if __name__ == '__main__':
    unittest.main()
