# 1. Задание
# Написать консольную программу используя классы.Выбор программы на ваше усмотрение.
# Как вариант можно написать консольную игру по типу поединок.
# Важно! Должно использоваться наследование

class Character:
    def __init__(self, gender, power, health, endurance):
        self.gender = gender
        self.power = power
        self.health = health
        self.endurance = endurance

    def say_gender(self):
        print(f'Gender: {self.gender}')

    def say_power(self):
        print(f'Power: {self.gender}')

    def say_health(self):
        print(f'Health: {self.gender}')

    def say_endurance(self):
        print(f'Endurance: {self.gender}')


class Hero(Character):
    def __init__(self, gender, power, health, endurance, name):
        super().__init__(gender, power, health, endurance)
        self.name = name

    def say_name(self):
        print(f'Name is: {self.name}')


support = Hero(name='Dawnbreaker', gender='female', power=57, health=720, endurance=50)
support.say_name()

mid = Hero(name='Zeus', gender='male', power=62, health=580, endurance=48)
mid.say_health()