import Lesson23_2
import unittest


class PalTest(unittest.TestCase):

    def setUp(self) -> None:
        print(f'Set up for [{self.shortDescription()}]')

    def tearDown(self) -> None:
        print(f"Tear down for [{self.shortDescription()}]")

    def test_result(self):
        """Result operation test"""
        self.assertEqual(Lesson23_2.result, Lesson23_2.result[::-1])

    @unittest.skipIf(Lesson23_2.num.isdigit(), "invalid literal")
    def test_exc(self):
        """Exception test. There is an invalid literal."""
        self.assertRaisesRegex(ValueError, "invalid literal", int, Lesson23_2.num)



if __name__ == '__main__':
    unittest.main()

