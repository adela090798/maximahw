import random

win = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, "empty"]]
game = list(range(16))
game[0] = "empty"
random.shuffle(game)
move = []
gameList = []

row = [game[0], game[1], game[2], game[3]]
row2 = [game[4], game[5], game[6], game[7]]
row3 = [game[8], game[9], game[10], game[11]]
row4 = [game[12], game[13], game[14], game[15]]
gameList.append(row)
gameList.append(row2)
gameList.append(row3)
gameList.append(row4)

for cell in gameList:
    print(cell)

for x in gameList:
    for y in x:
        if y == "empty":
            numRow = gameList.index(x)
            numClmn = x.index(y)
            indEmpty = gameList[numRow][numClmn]


def move():
    global numClmn
    global numRow
    global indEmpty
    while gameList != win:
        try:
            usernum = input("Какую кнопку двигать: ")
            usernum = int(usernum)
        except ValueError:
            print("Введите целое число в пределах игры(от 1 до 15)")
        if usernum in gameList[0]:
            userRow = 0
            userClmn = gameList[0].index(usernum)
            if userRow == numRow or userClmn == numClmn:
                if userRow + 1 == numRow or userRow - 1 == numRow or userClmn + 1 == numClmn or userClmn - 1 == numClmn:
                    gameList[numRow][numClmn], gameList[userRow][userClmn] = gameList[userRow][userClmn], gameList[numRow][numClmn]
                    numRow = userRow
                    numClmn = userClmn
                    for cell in gameList:
                        print(cell)
                else:
                    print("Эту кнопку нельзя двигать")
            else:
                print("Эту кнопку нельзя двигать")
        elif usernum in gameList[1]:
            userRow = 1
            userClmn = gameList[1].index(usernum)
            if userRow == numRow or userClmn == numClmn:
                if userRow + 1 == numRow or userRow - 1 == numRow or userClmn + 1 == numClmn or userClmn - 1 == numClmn:
                    gameList[numRow][numClmn], gameList[userRow][userClmn] = gameList[userRow][userClmn], gameList[numRow][numClmn]
                    numRow = userRow
                    numClmn = userClmn
                    for cell in gameList:
                        print(cell)
                else:
                    print("Эту кнопку нельзя двигать")
            else:
                print("Эту кнопку нельзя двигать")
        elif usernum in gameList[2]:
            userRow = 2
            userClmn = gameList[2].index(usernum)
            if userRow == numRow or userClmn == numClmn:
                if userRow + 1 == numRow or userRow - 1 == numRow or userClmn + 1 == numClmn or userClmn - 1 == numClmn:
                    gameList[numRow][numClmn], gameList[userRow][userClmn] = gameList[userRow][userClmn], gameList[numRow][numClmn]
                    numRow = userRow
                    numClmn = userClmn
                    for cell in gameList:
                        print(cell)
                else:
                    print("Эту кнопку нельзя двигать")
            else:
              print("Эту кнопку нельзя двигать")
        elif usernum in gameList[3]:
            userRow = 3
            userClmn = gameList[3].index(usernum)
            if userRow == numRow or userClmn == numClmn:
                if userRow + 1 == numRow or userRow - 1 == numRow or userClmn + 1 == numClmn or userClmn - 1 == numClmn:
                    gameList[numRow][numClmn], gameList[userRow][userClmn] = gameList[userRow][userClmn], gameList[numRow][numClmn]
                    numRow = userRow
                    numClmn = userClmn
                    for cell in gameList:
                        print(cell)
                else:
                    print("Эту кнопку нельзя двигать")
            else:
                print("Эту кнопку нельзя двигать")
        else:
            print("Введите целое число в пределах игры(от 1 до 15)")


move()
if gameList == win:
    print('Поздравляю! Вы победили!')
