# 2.Вернуть первое слово из строки с помощью регулярных выражений

import re

string = input("String: ")
word = re.match(r'\w+', string)
print(word.group(0))

#второй вариант
result = re.findall(r'^\w+', string)
print(result)
