
list1 = [i for i in range(1, 1000)]
list2 = []
result = 0
for i in list1:
    if i % 3 == 0 or i % 5 == 0:
        list2.append(i)
for num in list2:
    result += num
print(result)
