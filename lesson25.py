# 1. Написать скрипт скачивающий страницу https://docs.python-requests.org/en/latest/


import requests


r = requests.post('https://ru.wikipedia.org/wiki/Python')
print(r.status_code)
r.raise_for_status()
with open('index.html', 'wb') as file:
    for chunk in r.iter_content(chunk_size=50000):
        file.write(chunk)

