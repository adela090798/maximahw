# Выписав первые шесть простых чисел, получим 2, 3, 5, 7, 11 и 13.
# Очевидно, что 6-е простое число - 13.
# Какое число является 10001-м простым числом?

number = 10001
list1 = [2]
i = 3
while len(list1) != number:
    counter = 1
    x = 2
    while counter < 2 and x <= (i / 2):
        if i % x == 0:
            counter += 1
        x += 1
    if counter < 2:
        list1.append(i)
    i += 2
print(list1[number - 1])
