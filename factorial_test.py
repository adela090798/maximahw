import Lesson23
import unittest
import math


class FactTest(unittest.TestCase):

    def setUp(self) -> None:
        print(f'Set up for [{self.shortDescription()}]')

    def tearDown(self) -> None:
        print(f"Tear down for [{self.shortDescription()}]")

    def test_result(self):
        """Result operation test"""
        self.assertEqual(Lesson23.factorial, math.factorial(Lesson23.n))

    def test_type(self):
        """Type operation test"""
        self.assertIsInstance(Lesson23.num, int)


if __name__ == '__main__':
    unittest.main()
