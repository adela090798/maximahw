# 1. Дана строка python best language. с помощью регулярных выражений
# найти с какого по какой символ встречается слово python

import re

pattern = 'python'
text = 'python best language'

match = re.search(pattern, text)
s = match.start()
e = match.end()
print(f" c {s} по {e}")
