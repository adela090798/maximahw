# 3. Написать программу на свободную тему с количеством методов не меньше 3
# и прописать необходимые тесты.
# ВАЖНО! Необходимо проверить все возможные ошибки через тесты и постараться обойти их



class Person:
    def __init__(self, name, age, height, weight):
        self.name = name
        self.age = age
        self.height = height
        self.weight = weight

    def say_name(self):
        return self.name

    def say_age(self):
        return self.age

    def say_height(self):
        return self.height

    def say_weight(self):
        return self.weight

    def less(self):
        result = self.weight < self.height
        print(result)



tom = Person("Tom", 30, 190, 80)
tom.say_name()
tom.say_age()
tom.say_height()
tom.say_weight()
tom.less()


