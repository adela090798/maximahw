# 3.Создать базу данных рабочих для некого предприятия
# Наполнить таблицу разными способами (Имя, Фамилия, Возраст, Специальность, Образование, Стаж работы, Зарплата)
# Вывести всех рабочих отсортировав по стажу, возрасту, зп
# Вывести первых 5 рабочих
# Удалить любого рабочего


import sqlite3

con = sqlite3.connect('employers.db')

cur = con.cursor()

cur.execute('''CREATE TABLE employers(fname text, lname text, age int, specialty text,
            experience int, salary int)''')


cur.execute("INSERT INTO employers VALUES('Артур','Айнуров', 32, 'педиатор', 6, 25000)")
cur.execute("INSERT INTO employers VALUES('Айнур','Давлетшин', 43, 'терапевт', 10, 33000)")
cur.execute("INSERT INTO employers VALUES('Рустам','Биктемеров', 54, 'хирург', 24, 47000)")

user = ('Анатолий', 'Петров', 35, 'стоматолог', 8, 37000)
cur.execute("INSERT INTO employers VALUES(?, ?, ?, ?, ?, ?);", user)


users = [('Татьяна', 'Андреева', 41, 'офтальмолог', 11, 38000), ('Ольга', 'Конюхова', 25, 'медсестра',2, 17000)]
cur.executemany("INSERT INTO employers VALUES(?, ?, ?, ?, ?, ?);", users)

con.commit()


for row in cur.execute('SELECT * FROM employers ORDER BY experience'):
    print(row)
print(f"{'-' * 20}\n")

for row in cur.execute('SELECT * FROM employers ORDER BY age'):
    print(row)
print(f"{'-' * 20}\n")

for row in cur.execute('SELECT * FROM employers ORDER BY salary'):
    print(row)
print(f"{'-' * 20}\n")

cur.execute("SELECT * FROM employers")
result_many = cur.fetchmany(5)
print(result_many)


cur.execute("SELECT * FROM employers WHERE age<30")
print(cur.fetchall())
con.commit()


con.close()
